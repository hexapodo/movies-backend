<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * Movie
 *
 * @ORM\Table(name="movie", indexes={@Index(name="movie_id", columns={"movie_id"})})
 * @ORM\Entity
 */
class Movie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="movie_id", type="string", length=10, precision=0, scale=0, nullable=false, unique=true)
     */
    private $movieId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Rank", mappedBy="movie")
     */
    private $ranks;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ranks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set movieId.
     *
     * @param string $movieId
     *
     * @return Movie
     */
    public function setMovieId($movieId)
    {
        $this->movieId = $movieId;

        return $this;
    }

    /**
     * Get movieId.
     *
     * @return string
     */
    public function getMovieId()
    {
        return $this->movieId;
    }

    /**
     * Add rank.
     *
     * @param \AppBundle\Entity\Rank $rank
     *
     * @return Movie
     */
    public function addRank(\AppBundle\Entity\Rank $rank)
    {
        $this->ranks[] = $rank;

        return $this;
    }

    /**
     * Remove rank.
     *
     * @param \AppBundle\Entity\Rank $rank
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRank(\AppBundle\Entity\Rank $rank)
    {
        return $this->ranks->removeElement($rank);
    }

    /**
     * Get ranks.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRanks()
    {
        return $this->ranks;
    }
}
