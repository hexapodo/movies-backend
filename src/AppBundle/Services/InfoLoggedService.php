<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use AppBundle\Services\JwtService;
use AppBundle\Entity\User;

/**
 * Returns relevant info from the logged user
 * @Author Juan Leon
 * 
 */
class InfoLoggedService {

    private $container;
    private $jwt;

    public function __construct(Container $container, JwtService $jwt) {
        $this->container = $container;
        $this->jwt = $jwt;
    }

    public function getInfo($token) {
        $userId = $this->jwt->getUserId($token);
        $em = $this->container->get('doctrine')->getManager();
        $usersRepo = $em->getRepository('AppBundle:User');
        /* @var $user User */
        $user = $usersRepo->find($userId);

        $info = array(
            "id" => $user->getId(),
            "name" => $user->getName(),
            "username" => $user->getUsername(),
        );
        
        return $info;
    }

}
