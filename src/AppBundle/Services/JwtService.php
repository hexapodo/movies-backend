<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use Firebase\JWT\JWT;

/**
 * Allow to handle authentication with json web tokens
 * @Author Juan Leon
 * 
 */
class JwtService {

    private $key;
    private $key2;

    public function __construct(Container $container) {
        $this->key = $container->getParameter('key');
        $this->key2 = $container->getParameter('key2');
    }

    /**
     * 
     * @param array $payload
     * @return string token jwt
     * @throws \Exception
     */
    public function generateToken(array $payload = array()) {
        if (is_array($payload) && isset($payload['custom'])) {
            $payload['custom'] = $this->encrypt($payload['custom']);
        } else {
            throw new \Exception('You must include custom key.');
        }
        $payload['time'] = microtime();
        return JWT::encode($payload, $this->key);
    }

    /**
     * 
     * @param string $token token to decode
     * @return stdClass object contained into the token
     */
    public function decode($token) {
        return JWT::decode($token, $this->key, array('HS256'));
    }

    /**
     * 
     * @param string $token
     * @return string Obtains the user id from the token
     */
    public function getUserId($token) {
            $object = $this->decode($token);
            $userId = (int)$this->decrypt($object->custom);
        return $userId;
    }

    private function encrypt($string) {
        $encrypted=base64_encode(openssl_encrypt($string,"AES-256-ECB", md5($this->key2), OPENSSL_RAW_DATA));
        return $encrypted; 
    }

    private function decrypt($string) {
        $decrypted=openssl_decrypt(base64_decode($string),"AES-256-ECB", md5($this->key2), OPENSSL_RAW_DATA);
        return $decrypted;  
    }

}
