<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use AppBundle\Services\JwtService;
use AppBundle\Entity\User;

/**
 * helper to build the movie uri
 * @Author Juan Leon
 * 
 */
class UrlMovieResolverService {

    private $urlBase;
    private $key;
    private $config;

    public function __construct(Container $container) {
        $this->urlBase = $container->getParameter('url_movie_api');
        $this->key = $container->getParameter('movie_api_key');
        $this->config = $container->getParameter('movie_api_config');
    }

    public function getFullUri($endpoint, $defaultKey, $parameters = []) {
        $strParams = 'sort_by=' . $this->config['sort'] . '&';

        if ($defaultKey !== null) {
            $parameters = array_merge($this->config['default_fields'][$defaultKey], $parameters);
        }
        // concatenates the params
        foreach ($parameters as $paramKey => $parameter) {
            $strParams .=  $paramKey . '=' . $parameter . '&';
        };
        
        $fullUri = $this->urlBase 
                . $endpoint
                . '?api_key=' . $this->key . '&'
                . $strParams;
        
        return $fullUri;
    }

}
