<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Services\JwtService;
use AppBundle\Services\UrlMovieResolverService;
use AppBundle\Entity\Movie;
use AppBundle\Entity\Rank;
use Symfony\Component\HttpClient\HttpClient;


class RankController extends FOSRestController
{

    /**
     * @Rest\Delete("/api/rank/{movieId}")
     */
    public function rankDeleteAction(Request $request, $movieId)
    {
        try {
            $tokeyKey = $this->container->getParameter('tokenKey');
            $token = $request->headers->get($tokeyKey);
            /* @var $infoLoggedUser InfoLoggedService */
            $infoLoggedUser = $this->get('infoLoggedUser');
            $info = $infoLoggedUser->getInfo($token);
        } catch (\Exception $e) {
            return new view(array(
                'message' => 'An internal error is present.',
                'error' => $e->getMessage(),
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR
            ), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
        $em = $this->getDoctrine()->getManager();
        $moviesRepo = $em->getRepository('AppBundle:Movie');
        
        // searches a given movie 
        /* @var $movieFromDb Movie */
        $movieFromDb = $moviesRepo->findOneBy(array('movieId' => $movieId));
        
        $ranks = $movieFromDb->getRanks();

        // searches ranks that belong logged user in the given movie
        /* @var $rank Rank */
        foreach ($ranks as $rank) {
            if ($rank->getUser()->getId() === $info['id']) {
                $em->remove($rank);
            }
        }
        $em->flush();
        
        // Deletes if the movie doesn't have ranks
        if (count($movieFromDb->getRanks())===0) {
            $em->remove($movieFromDb);
        }
        $em->flush();
        
        return new view(array(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\Post("/api/rank")
     */
    public function rankCreationAction(Request $request)
    {
        try {
            $tokeyKey = $this->container->getParameter('tokenKey');
            $token = $request->headers->get($tokeyKey);
            /* @var $infoLoggedUser InfoLoggedService */
            $infoLoggedUser = $this->get('infoLoggedUser');
            $info = $infoLoggedUser->getInfo($token);
        } catch (\Exception $e) {
            return new view(array(
                'message' => 'An internal error is present.',
                'error' => $e->getMessage(),
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR
            ), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $em = $this->getDoctrine()->getManager();
        $moviesRepo = $em->getRepository('AppBundle:Movie');
        $usersRepo = $em->getRepository('AppBundle:User');
        $user = $usersRepo->find($info['id']);
                
        $comment = $request->request->get('comment', '');
        $ranking = $request->request->get('rank', 0);
        $movieId = $request->request->get('movieId', 0);
        
        // searches a given movie 
        /* @var $movieFromDb Movie */
        $movieFromDb = $moviesRepo->findOneBy(array('movieId' => $movieId));
        
        if (!$movieFromDb) {
            $movieFromDb = new movie();
            $movieFromDb->setMovieId($movieId);
            $em->persist($movieFromDb);
            $em->flush();
        }
        $rank = new Rank();
        $rank->setComment($comment);
        $rank->setRank($ranking);
        $rank->setMovie($movieFromDb);
        $rank->setUser($user);
        $em->persist($rank);
        $em->flush();
        return new view(array(
            'rankId' => $rank->getId()
        ), Response::HTTP_CREATED);
    }
    
    /**
     * @Rest\Put("/api/rank/{rankId}")
     */
    public function rankModificationAction(Request $request, $rankId)
    {
        try {
            $tokeyKey = $this->container->getParameter('tokenKey');
            $token = $request->headers->get($tokeyKey);
            /* @var $infoLoggedUser InfoLoggedService */
            $infoLoggedUser = $this->get('infoLoggedUser');
            $info = $infoLoggedUser->getInfo($token);
        } catch (\Exception $e) {
            return new view(array(
                'message' => 'An internal error is present.',
                'error' => $e->getMessage(),
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR
            ), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $em = $this->getDoctrine()->getManager();
        $ranksRepo = $em->getRepository('AppBundle:Rank');
  
        $comment = $request->request->get('comment', '');
        $ranking = $request->request->get('rank', 0);
        /* @var $rank Rank */
        $rank = $ranksRepo->find($rankId);
        // Security issue was fixed,
        // the user who is trying to change the ranking/comment
        // is the owner of that record
        if ($rank && $rank->getUser()->getId() === $info['id'])
        {
            $rank->setComment($comment);
            $rank->setRank($ranking);
            $em->persist($rank);
            $em->flush();
        } else {
            return new view(array(
                'message' => 'Access denied',
                'error' => 'HTTP_FORBIDDEN'
            ), Response::HTTP_FORBIDDEN);
        }
        return new view(array(
            'rankId' => $rank->getId()
        ), Response::HTTP_ACCEPTED);
    }

}
