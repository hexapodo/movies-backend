<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Services\JwtService;
use AppBundle\Services\UrlMovieResolverService;
use AppBundle\Entity\Movie;
use AppBundle\Entity\Rank;
use Symfony\Component\HttpClient\HttpClient;


class MovieController extends FOSRestController
{

    /**
     * @Rest\Get("/api/movie/year/{year}")
     */
    public function movieByYearAction(Request $request, $year)
    {
        try {
            $tokeyKey = $this->container->getParameter('tokenKey');
            $token = $request->headers->get($tokeyKey);
            /* @var $infoLoggedUser InfoLoggedService */
            $infoLoggedUser = $this->get('infoLoggedUser');
            $info = $infoLoggedUser->getInfo($token);
        } catch (\Exception $e) {
            return new view(array(
                'message' => 'An internal error is present.',
                'error' => $e->getMessage(),
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR
            ), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $page = $request->query->get('page', 1);
        
        $url_imgs = $this->getParameter('url_movie_imgs');

        /* @var $urlMovieResolver UrlMovieResolverService */
        $urlMovieResolver = $this->get('urlMovieResolver');
        
        $url = $urlMovieResolver->getFullUri('discover/movie', 'list', array(
            'page' => $page,
            'primary_release_year' => $year
        ));
        $client = HttpClient::create();
        $response = $client->request('GET', $url);
        $statusCode = $response->getStatusCode();
        if ($statusCode === 404) {
            return new view(array(
                'message' => 'Resource not found'
            ), Response::HTTP_NOT_FOUND);
        }
        if ($statusCode === 401) {
            return new view(array(
                'message' => 'Unauthorized access'
            ), Response::HTTP_UNAUTHORIZED);
        }
        $content = $response->toArray();
        $results = array_map(function($result) use ($url_imgs) {
            return array(
                'id' => $result['id'],
                'title' => $result['title'] ? $result['title'] : $result['original_title'],
                'date' => $result['release_date'],
                'poster' => $result['poster_path'] ? $url_imgs . $result['poster_path'] : null,
                'description' => $result['overview']
            );
        }, $content['results']);
        // overwrite the results to simplify the response
        $content['results'] = $results;

        return new view($content, Response::HTTP_OK);        
    }
    
    /**
     * @Rest\Get("/api/movie/{movieId}")
     */
    public function movieByIdAction(Request $request, $movieId)
    {
        try {
            $tokeyKey = $this->container->getParameter('tokenKey');
            $token = $request->headers->get($tokeyKey);
            /* @var $infoLoggedUser InfoLoggedService */
            $infoLoggedUser = $this->get('infoLoggedUser');
            $info = $infoLoggedUser->getInfo($token);
        } catch (\Exception $e) {
            return new view(array(
                'message' => 'An internal error is present.',
                'error' => $e->getMessage(),
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR
            ), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
        $em = $this->getDoctrine()->getManager();
        $moviesRepo = $em->getRepository('AppBundle:Movie');
        
        $url_imgs = $this->getParameter('url_movie_imgs');

        /* @var $urlMovieResolver UrlMovieResolverService */
        $urlMovieResolver = $this->get('urlMovieResolver');
        
        $url = $urlMovieResolver->getFullUri('movie/' . $movieId, null);
        $client = HttpClient::create();
        $response = $client->request('GET', $url);
        $statusCode = $response->getStatusCode();
        if ($statusCode === 404) {
            return new view(array(
                'message' => 'Resource not found'
            ), Response::HTTP_NOT_FOUND);
        }
        if ($statusCode === 401) {
            return new view(array(
                'message' => 'Unauthorized access'
            ), Response::HTTP_UNAUTHORIZED);
        }
        $content = $response->toArray();
        /* @var $movieFromDb Movie */
        $movieFromDb = $moviesRepo->findOneBy(array('movieId' => $content['id']));
        $ranks = array();
        
        if ($movieFromDb) {
            /* @var $rankFromDb Rank */
            foreach ($movieFromDb->getRanks() as $rankFromDb) {
                $ranks[] = array(
                    'id' => $rankFromDb->getId(),
                    'rank' => $rankFromDb->getRank(),
                    'comment' => $rankFromDb->getComment(),
                    'user' => array(
                        'id' => $rankFromDb->getUser()->getId(),
                        'name' => $rankFromDb->getUser()->getName(),
                        'username' => $rankFromDb->getUser()->getUsername(),
                    )
                );
            }

            // Gets the rank and comment of the logged user
            $myRank = array_values(array_filter($ranks, function($rank) use ($info){
                return $rank['user']['id'] === $info['id'];
            }));

            // Gets the ranks and comments from all user except the one from the logged user
            $communityRanks = array_values(array_filter($ranks, function($rank) use ($info){
                return $rank['user']['id'] !== $info['id'];
            }));

            // obtains the average of rank from all the users
            if (count($ranks) === 0) {
                $rank = null;
            } else {
                $rank = array_reduce($ranks, function($acc, $rank) {
                    return $acc + $rank['rank'];
                }, 0)/count($ranks);
            }
        } else {
            $myRank = [];
            $communityRanks = [];
            $rank = 0;
        }

        $movie = array(
            'id' => $content['id'],
            'title' => $content['title'] ? $content['title'] : $content['original_title'],
            'date' => $content['release_date'],
            'rank' => $rank,
            'poster' => $content['poster_path'] ? $url_imgs . $content['poster_path'] : null,
            'description' => $content['overview'],
            'myRank' => (count($myRank) > 0) ? $myRank[0] : null,
            'community' => $communityRanks
        );

        return new view($movie, Response::HTTP_OK);        
    }
    
}
