<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Services\JwtService;
use AppBundle\Entity\User;

class DefaultController extends FOSRestController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
    
    /**
     * @Rest\Post("/api/login")
     */
    public function loginAction(Request $request)
    {
        $username = $request->request->get('username', null);
        $password = $request->request->get('password', null);

        /* @var $jwt JwtService */
        $jwt = $this->get('jwt');
        
        $em = $this->getDoctrine()->getManager();
        $usersRepo = $em->getRepository('AppBundle:User');
        /* @var $user User */
        $user = $usersRepo->findOneBy(array(
                'username' => $username
            ));
        if ($user) {
            $token = $jwt->generateToken(array("custom" => $user->getId()));
            
            /* @var $infoLoggedUser InfoLoggedService */
            $infoLoggedUser = $this->get('infoLoggedUser');
            $info = $infoLoggedUser->getInfo($token);
            unset($info['id']);
            
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
            $encodedPassword = $encoder->encodePassword($password, $user->getSalt());
            if ($user->getPassword() === $encodedPassword) {
                // valid user & pass
                return new view(array(
                    "token" => $token,
                    "user" => $info
                ), Response::HTTP_OK);
            } else {
                // password doesn't match
                return new view(array(
                    'message' => 'Unauthorized'
                        ), Response::HTTP_UNAUTHORIZED);
            }
        } else {
            return new view(array(
                'message' => 'Unauthorized'
                    ), Response::HTTP_UNAUTHORIZED);
        }
    }
    
}
